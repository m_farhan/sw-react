import React from 'react';
import './SearchView.css'
const SearchView = (props) => {
    return(
        <div className="search-input">
                    <label>{props.label}</label>
                    <div className={'input-pan'}>
                    <input type='text' onChange={props.onChange} value={props.value}/>
                    <button onClick={props.onSubmit}><i className="fas fa-search"></i></button>
                    </div>
            </div>
    );
};

export default SearchView;