import React from 'react';
import './Layout.css'
import {Panel, Grid, Row, Col} from 'react-bootstrap'
const Layout = (props) => {
    return(
        <div>
            <div className={'header'}>
            <Grid>
                <Row>
                    <Col sm={6}>
                    <img  className="img " src="https://www.sunwing.ca/Content/images/global/header/sunwing-experience-the-difference-white-logo.png"></img>
                    </Col>
                    <Col  sm={6}>
                    </Col>
                </Row>
            </Grid>
            </div>
            <div className={"boddy"}>
                   {props.children}
            </div>
            <div className={'footer'}>
            </div>
            </div>
    );
};

export default Layout;