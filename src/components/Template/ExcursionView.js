import React from 'react';
import './ExcursionView.css'
import uuidv1 from  'uuid/v1';
const ExcursionView = (props) => {
    return(
        <div className="excursion"  key={uuidv1()}>
                    <div className={'head'}>
                    <h3>{props.ex.excursionFullName}</h3>
                    <label>{props.ex.categoryName} | {props.ex.subCategoryName}</label>
                    </div>
                    <div className={'body'}>
                    <table style={{width:'100%'}}>
                    <tbody>
                        <tr>
                            <td style={{width:'275px'}}>

                        {props.ex.excursionImages!=null?<img src={props.ex.excursionImages.Img4X3[0]} />:<img href={''} />}
                            </td>
                            <td style={{paddingRight:'20px',verticalAlign:'top'}}>
                                
                        {props.ex.excursionShortDescription}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </div>
            </div>
    );
};

export default ExcursionView;