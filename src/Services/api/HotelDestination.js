import request from "../../util/request"

function getAll() {
    return request({
      url:    '/en/AllHotelDestinationList',
      method: 'GET'
    });
  }
  const HotelDestinationsService={getAll}
  export default HotelDestinationsService