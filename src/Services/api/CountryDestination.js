import request from "../../util/request"

function getAll(country,destination) {
    return request({
      url:    `/1/en/excursionsCountryDestination/${country}/${destination}`,
      method: 'GET'
    });
  }
  const CountryDestinationsService={getAll}
  export default CountryDestinationsService