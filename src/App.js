import React, { Component } from 'react';
import './App.css';
import Axios from "axios";
import ExcrusionPage from './container/ExcursionPage'
import CountriesPage from './container/CountriesPage'
import Layout from './components/Template/Layout'
import { BrowserRouter,Route, Switch } from 'react-router-dom';
Axios.defaults.baseURL = 'https://hotelinfoservice.sunwingtravelgroup.com';
class App extends Component {
  render() {
    return (
      <Layout>
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={CountriesPage} />
            <Route path="/:country/:dest" component={ExcrusionPage} />
          </Switch>
        </BrowserRouter>
      </Layout>
    );
  }
}

export default App;
