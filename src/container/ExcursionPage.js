import React, { Component } from 'react';
import _ from 'lodash'
import CountryDestinationsService from '../Services/api/CountryDestination';
import CategoryFilter from './CategoryFilter'
import {Panel, Grid, Row, Col} from 'react-bootstrap'
import ExcursionView from '../components/Template/ExcursionView'
import uuidv1 from  'uuid/v1';
class CatagoryFilter extends Component {
  constructor(props){
    super(props);
    this.state= {
      data:[],
      categories:[],
    }
    var searchTitle='';
    var iniData=null;
    this.onFilterChange=this.onFilterChange.bind(this);
    this.onTitleSearchChange=this.onTitleSearchChange.bind(this);
  }
  /*
Call back function for search word 
  */
  onTitleSearchChange(categories,search)
  {
    this.searchTitle=search;
    this.onFilterChange(categories);
  }

  /* 
  Call back function for filters and called every time filters change
  */
  onFilterChange(categories)
  {
    var data=_.cloneDeep(this.iniData);
    const categoriesList=categories.map(e=>e.categoryName);
    data=_.filter(data,e=>( _.indexOf(categoriesList, e.categoryName)!=-1));
    const result=data.map(e=>{
      e.subCategories=_.filter(e.subCategories,i=>( _.indexOf(_.filter(categories,j=>j.categoryName==e.categoryName)[0].subCategoryName, i.subCategoryName)!=-1));
      return e;
    })
    this.setState({data:Object.assign(this.mapData(result,this.searchTitle))});
  }
  componentDidMount()
  {
    const _this=this;
    CountryDestinationsService.getAll(_this.props.match.params.country,_this.props.match.params.dest)
    .then(data=>{
        this.iniData={...data};       //Keep orignal data and filtered data separate
      var categories=[];
     data.map(e=>{
      var subCatagory=[];
       e.subCategories.map(i=>{
        subCatagory.push(i.subCategoryName);
       });
       categories.push({'categoryName':e.categoryName,'subCategoryName':subCatagory});
     });
     /*
     map data for filter options
     [
       {
          categoryName:AAAA,
          subCategoryName:[aaa,aaa,aaaa,aa]
       }

     ]

     */
     

      _this.setState({categories:Object.assign(categories),data:Object.assign(_this.mapData(data,this.searchTitle))});
});
  }

  /*
  flettened data into a single array to map on view 

  */
  mapData(d,search)
  {
    search=search||'';
    const data=_.cloneDeep(d);
    var excursions=[];
     _.forEach(data,category=>{
       if(category!=undefined)
       {
        _.forEach(category.subCategories,subCategory=>{
          _.forEach(subCategory.excursions,ex=>{
            if(ex.excursionFullName.toLowerCase().includes(search.toLowerCase()))
            {
            ex.categoryName=category.categoryName;
            ex.subCategoryName=subCategory.subCategoryName;
            excursions.push( ex)
            }
        });
        
        });
      }
       });
       
       return excursions;
  }
  render() {
    return (
      <div>
        <Grid>
          <Row>
            <Col sm={3}>

            <CategoryFilter categories={this.state.categories} onFilterChange={this.onFilterChange} onTitleSearchChange={this.onTitleSearchChange}/>
            </Col>
            <Col sm={9}>
            {this.state.data.map(ex=><ExcursionView ex={ex} key={uuidv1()}/>)}
            </Col>
          </Row>
         
    </Grid>
      </div>
    );
  }
}

export default CatagoryFilter;
