import React, { Component } from 'react';
import _ from 'lodash'
import {Panel, Grid, Row, Col} from 'react-bootstrap'
import SerachView from '../components/Template/Form/SearchView'
import './CategoryFilter.css'
import uuidv1 from  'uuid/v1';
class CategoryFilter extends Component {
  constructor(props){
    super(props);
    this.state= {
      filtered:null,
      categories:null,
      searchTitleValue:''
    }
    this.handleCatagoryChangeAction=this.handleCatagoryChangeAction.bind(this);
    this.handleSubCatagoryChangeAction=this.handleSubCatagoryChangeAction.bind(this);
    this.handleSearchValueChange=this.handleSearchValueChange.bind(this);
    this.handleTitleSearch=this.handleTitleSearch.bind(this);
  }
  /*
data design
[
       {
          categoryName:AAAA,
          subCategoryName:[aaa,aaa,aaaa,aa]
       }

     ]
  */
  /*
  handle title search button change and callback parent component
  */
  handleTitleSearch()
  {
    
    this.props.onTitleSearchChange(this.state.filtered,this.state.searchTitleValue);
  }
  /*
Handle serach value change to sync value with parent 
  */
  handleSearchValueChange(e)
  {
    this.setState({searchTitleValue:e.target.value});
  }
  /*
Handle when sub category input check box changed  
  */
  handleSubCatagoryChangeAction(event,category,subCategory)
  {
    const target = event.target;
    var catagory=_.cloneDeep(_.filter(this.state.filtered,e=>e.categoryName==category)[0]);//get category object need to be changed
    var _filtered=_.cloneDeep(_.filter(this.state.filtered,e=>e.categoryName!=category));//get categories data with out object need to be changed
    if(target.checked)
    {                                                                                 //push category into array and update filtered array data and update state
      catagory.subCategoryName.push(subCategory)
      this.setState({filtered:[..._filtered,catagory]});
      this.props.onFilterChange([..._filtered,catagory]);
    }
    else{
                                                                                      // remove sub category from array and push back object in data and update state
      catagory.subCategoryName.splice( catagory.subCategoryName.indexOf(subCategory), 1 )
      this.setState({filtered:[..._filtered,catagory]});
      this.props.onFilterChange([..._filtered,catagory]);
    }

  }
  /*
Handle wehen category changed  
  */
  handleCatagoryChangeAction(event,category){
    
    const target = event.target;
    var _filtered=_.cloneDeep(this.state.filtered);  // careate immuteable copy of filtered data
    var _categories=_.cloneDeep(this.state.categories);  // careate immuteable copy of orignal data
    if(target.checked)
    {
      this.setState({filtered:_filtered.concat(_.filter(_categories,e=>e.categoryName==category))}); //add compleate object of cetagory in filter data and update state and parent 
      this.props.onFilterChange(_filtered.concat(_.filter(_categories,e=>e.categoryName==category)));
    }
    else{
      
      this.setState({filtered:_.filter(_filtered,e=>e.categoryName!=category)});//remove compleate object of cetagory in filter data and update state and parent
      this.props.onFilterChange(_.filter(_filtered,e=>e.categoryName!=category));
    }
  }
 componentWillUpdate()
 {
  if(this.state.categories==null)
  {
    this.setState({filtered:_.cloneDeep(this.props.categories),categories:_.cloneDeep(this.props.categories)});
  } 
 }
  render() {
    return (
      <div className={'filter-pan'}>
          <SerachView label='Search Name' onChange={this.handleSearchValueChange} value={this.state.searchTitleValue} onSubmit={this.handleTitleSearch}/>
        {
        <Panel id="collapsible-panel-example-2">
        <Panel.Heading>
          <Panel.Title toggle>
            Categories name
          </Panel.Title>
        </Panel.Heading>
        <Panel.Collapse>
          <Panel.Body>
          {this.state.categories==null?'':this.state.categories.map(e => {
            var categorySlected=_.find(this.state.filtered,{'categoryName':e.categoryName} );
            return (<div className='category' key={uuidv1()}><b><input type='checkbox' checked={categorySlected!=undefined} onChange={event=>this.handleCatagoryChangeAction(event,e.categoryName)} /> {e.categoryName}</b>
            {e.subCategoryName.map(i=>
            <div  className='sub-category' key={uuidv1()}><input type='checkbox' checked={categorySlected!=undefined?categorySlected.subCategoryName.indexOf(i)!=-1:false} disabled={categorySlected==undefined} onChange={event=>this.handleSubCatagoryChangeAction(event,e.categoryName,i)} />{i}</div>)}
            </div>)
        })}
         </Panel.Body>
          </Panel.Collapse>
        </Panel>
      }
      </div>
    );
  }
}

export default CategoryFilter;
