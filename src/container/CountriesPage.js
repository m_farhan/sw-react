import React, { Component } from 'react';
import _ from 'lodash'
import HotelDestination from '../Services/api/HotelDestination';
import Select from 'react-select'
import {Panel, Grid, Row, Col} from 'react-bootstrap'
import { Link} from 'react-router-dom';
import './CountriesPage.css'
class CountriesPage extends Component {
  constructor(props){
    super(props);
    this.state= {
      data:{},
      availableDestivations:[],
      currentDestination:null,
    }
    this.handleChange=this.handleChange.bind(this);
  }
  handleChange(e)
  {
    this.setState({currentDestination:e});
    
  }
  componentDidMount()
  {
    const _this=this;
    HotelDestination.getAll()
    .then(data=>{
      var dest=[];
      data.map(e=>{
        dest.push({value:{country:e.countryName,dest:null}, label:<b>{e.countryName}</b>})
        e.destinations.map(i=>{
          dest.push({value:{country:e.countryName,dest:i}, label:<span>{i}</span>})
        })
      });
      /*
      map data into for (select React)
      [
        {country:AAA,dest:AAA},
        {country:AAA,dest:null}
      ]
      */

     _this.setState({data:{...data},availableDestivations:_.cloneDeep(dest)});
});
  }
  render() {
    return (
      
        <Grid>
          <div className={'search'}>
          <Row>
            <Col sm={6}>
            <div style={{maxWidth:'200px'}}>
            <label> Select Destination</label>
        <Select
            value={this.state.currentDestination}
            name="country"
            onChange={this.handleChange}
            options={this.state.availableDestivations}
          />
              </div> 
            </Col>
            
            <Col sm={6} style={{textAlign:'right'}}>
            {this.state.currentDestination==null?'':this.state.currentDestination.value.dest==null?'':<Link to={'/'+this.state.currentDestination.value.country+'/'+this.state.currentDestination.value.dest} className={'search-button'} disabled={this.state.currentDestination.value.dest==null}> Search </Link>}
            </Col>
          </Row>
      </div>
        </Grid>
       
         
    );
  }
}

export default CountriesPage;
